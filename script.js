const gameBoard = document.getElementById("gameBoard");
const player = document.getElementById("player");
const winSpan = document.getElementById("winSpan");
const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

let dataModel = [];

function createMap(map) {
    for (let rowIndex = 0; rowIndex < map.length; rowIndex++) {
        const rowModel = [];
        const rowView = document.createElement("div");
        rowView.className = "row";
        rowView.id = ("row" + rowIndex);
        gameBoard.appendChild(rowView);
        dataModel.push(rowModel);
        for (let columnIndex = 0; columnIndex < map[rowIndex].length; columnIndex++) {
            const cell = document.createElement("div");
            cell.className = "cell";
            cell.id = ("cell" + rowIndex + "-" + columnIndex);
            cell.dataset.type = (map[rowIndex][columnIndex])
            rowView.appendChild(cell);
            rowModel.push(cell);
        }
    }
    dataModel[9][0].appendChild(player);
}
function movePlayer(map, direction) {
    let currentColumn = findCurrentColumn(map);
    let currentRow = findCurrentRow(map);
    if (direction === "ArrowUp") {
        map[currentRow - 1][currentColumn].appendChild(player);
    }
    if (direction === "ArrowDown") {
        map[currentRow + 1][currentColumn].appendChild(player);
    }
    if (direction === "ArrowLeft") {
        map[currentRow][currentColumn - 1].appendChild(player);
    }
    if (direction === "ArrowRight") {
        map[currentRow][currentColumn + 1].appendChild(player);
    }
}

function findCurrentColumn(map) {
    for (let rowIndex = 0; rowIndex < map.length; rowIndex++) {
        for (let columnIndex = 0; columnIndex < map[rowIndex].length; columnIndex++) {
            let currentCell = map[rowIndex][columnIndex]
            if (currentCell.lastElementChild === player) { return (columnIndex) }
        }
    }
}
function findCurrentRow(map) {
    for (let rowIndex = 0; rowIndex < map.length; rowIndex++) {
        for (let columnIndex = 0; columnIndex < map[rowIndex].length; columnIndex++) {
            let currentCell = map[rowIndex][columnIndex]
            if (currentCell.lastElementChild === player) { return (rowIndex) }
        }
    }
}

function checkForWall(map, direction){
    let currentColumn = findCurrentColumn(map);
    let currentRow = findCurrentRow(map);
    if  ((direction === "ArrowUp")&&(map[currentRow -1][currentColumn].dataset.type === "W")) {return("illegal move")}
    else if ((direction === "ArrowDown")&&(map[currentRow + 1][currentColumn].dataset.type === "W")) {return("illegal move")}
    else if ((direction === "ArrowLeft")&&(map[currentRow][currentColumn - 1].dataset.type === "W")) {return("illegal move")}
    else if ((direction === "ArrowRight")&&(map[currentRow][currentColumn + 1].dataset.type === "W")) {return("illegal move")}
}

function checkForWin(map){
    let currentColumn = findCurrentColumn(map);
    let currentRow = findCurrentRow(map);
    if ((currentRow === 8 ) && (currentColumn === 20)) {winSpan.innerText = "You Win"}
}

createMap(map);

document.addEventListener("keydown", function (event) {
    checkForWin(dataModel);
    if (event.key === "ArrowUp") {
        if (checkForWall(dataModel, "ArrowUp") === "illegal move"){return}
        movePlayer(dataModel, "ArrowUp");
    }
    if (event.key === "ArrowDown") {
        if (checkForWall(dataModel, "ArrowDown") === "illegal move"){return}
        movePlayer(dataModel, "ArrowDown");
    }
    if (event.key === "ArrowLeft") {
        if (checkForWall(dataModel, "ArrowLeft") === "illegal move"){return}
        movePlayer(dataModel, "ArrowLeft");
    }
    if (event.key === "ArrowRight") {
        if (checkForWall(dataModel, "ArrowRight") === "illegal move"){return}
        movePlayer(dataModel, "ArrowRight");
    }
});