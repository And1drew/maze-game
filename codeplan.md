const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

*   Make each cell of the maze a DIV.
    use nested for loops to cycle through the array and display either "Space" (passable floor), "W" (impassable wall), "S" (starting postion), or "F" (Finish postition);
        Make each index a div for each row
        ===> Done

*   Make each row of the maze a DIV using "display: flex;"
    ===> Done

*   Either A) use an absolutely-positioned DIV to represent the player's current position in the maze, or B) have your player DIV appended to a cell DIV for the same reason.
    

*    You need to keep track of (or retrieve on demand) the player's current position in the maze (row index and column index). You could do this one of several ways. You could keep a persistent record of the player's position in, say, a global array or object whose sole job is keeping track of the player's current position. You could constantly update your map array to reflect your player's movement (move the "S" around). You could keep your indexes in data attributes in your HTML and access them through player DIV's "parentElement" property (in the case of 3B). Or you could do a little math on the player DIV's current position on the screen, relative to the start element's current position on the screen and the size of your cells (in the case of 3A).


*   Movement can be performed a couple different ways: In the case of 3A, change the absolute position of the player DIV. Or, in the case of 3B, append the player DIV to the next cell DIV. (You could use "document.querySelector()", and the CSS selector for attributes to get the next cell element by the indexes you set on it via data attributes.)

game board made ===> Done
    function to get starting postion
        function to move starting position with arrow keys
            function to check for walls
                function to check if current postition is at the end postion (win condition)
                    if win condition is met display message and option to restart game


 function moveColumnPosition(map,direction){
     if (direction === "left"){
        //column postion -1
     } else if (direction === "right"){
        // findCurrentcolumn(map) + 1
     }
     return map;
 }

 function moveRowPosition(map,direction){
     if (direction === "up"){
        // row postion -1
     } else if (direction === "down"){
        // row position +1
     }
     return map;
 }